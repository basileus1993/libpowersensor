#ifndef AMDGPU_POWER_SENSOR_H
#define AMDGPU_POWER_SENSOR_H

#include "PowerSensor.h"

namespace powersensor {
    namespace amdgpu {
        class AMDGPUPowerSensor : public PowerSensor {
            public:
                static AMDGPUPowerSensor* create(const unsigned device_number, const char *dumpFileName = NULL);
        };
    } // end namespace amdgpu
} // end namespace powersensor

#endif
