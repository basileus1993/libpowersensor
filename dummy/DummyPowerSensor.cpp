#include <omp.h>

#include "DummyPowerSensor.h"

namespace powersensor {

class DummyPowerSensor_ : public DummyPowerSensor {
    public:
        virtual State read();
};

DummyPowerSensor* DummyPowerSensor::create()
{
    return new DummyPowerSensor_();
}

State DummyPowerSensor_::read() {
    State state;
    state.timeAtRead = omp_get_wtime();
    state.joulesAtRead = 0;
    return state;
}

} // end namespace powersensor
