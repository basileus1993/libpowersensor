#ifndef DUMMY_POWER_SENSOR_H_
#define DUMMY_POWER_SENSOR_H_

#include "PowerSensor.h"

namespace powersensor {
    class DummyPowerSensor : public PowerSensor {
        public:
            static DummyPowerSensor* create();
    };
} // end namespace powersensor

#endif
