#include "powersensor-config.h"

#include "powersensor/PowerSensor.h"
#include "powersensor/DummyPowerSensor.h"
#include "powersensor/ArduinoPowerSensor.h"
#if defined(HAVE_LIKWID)
#include "powersensor/LikwidPowerSensor.h"
#endif
#include "powersensor/RaplPowerSensor.h"
#if defined(HAVE_NVML)
#include "powersensor/NVMLPowerSensor.h"
#endif
#include "powersensor/AMDGPUPowerSensor.h"
