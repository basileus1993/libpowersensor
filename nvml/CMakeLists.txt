project(powersensor-nvml)

add_library(
    ${PROJECT_NAME}
    OBJECT
    NVMLPowerSensor.cpp
)

target_link_libraries(
    ${PROJECT_NAME}
    ${NVML_LIBRARY}
)

include_directories(
    ${NVML_INCLUDE_DIR}
)

set_target_properties(
    ${PROJECT_NAME}
    PROPERTIES
    COMPILE_FLAGS "-fPIC"
)

install(
    FILES
    NVMLPowerSensor.h
    DESTINATION
    include/powersensor
)

# NVMLPowerMeter
add_executable(
    NVMLPowerMeter
    NVMLPowerMeter.cpp
)

target_link_libraries(
    NVMLPowerMeter
    powersensor-nvml
    powersensor-common
)