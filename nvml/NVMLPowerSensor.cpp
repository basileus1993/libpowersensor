#include "NVMLPowerSensor.h"

#include <iostream>
#include <stdexcept>
#include <sstream>

#include <unistd.h>
#include <omp.h>

#if defined(HAVE_NVML)
#include "nvml.h"

#define checkNVMLCall(val)  __checkNVMLCall((val), #val, __FILE__, __LINE__)

inline void __checkNVMLCall(
    nvmlReturn_t result,
    const char *const func,
    const char *const file,
    int const line)
{
    if (result != NVML_SUCCESS) {
        std::stringstream error;
        error << "NVML Error at " << file;
        error << ":" << line;
        error << " in function " << func;
        error << ": " << nvmlErrorString(result);
        error << std::endl;
        throw std::runtime_error(error.str());
    }
}
#endif


namespace powersensor {
namespace nvml {

class NVMLPowerSensor_ : public NVMLPowerSensor {
    public:
        NVMLPowerSensor_(const int device_number, const char *dumpFileName);
        ~NVMLPowerSensor_();
        State read();

    private:
        class NVMLState {
            public:
                operator State();
                double timeAtRead;
                unsigned int instantaneousPower   = 0;
                unsigned int consumedEnergyDevice = 0;
        };

    private:
        // Thread
        pthread_t     thread;
        volatile bool stop;
        static void   *IOthread(void *);
        void          *IOthread();

        // State
        NVMLState previousState;
        NVMLState read_nvml();

#if defined(HAVE_NVML)
        nvmlDevice_t device;
#endif
};

NVMLPowerSensor_::NVMLState::operator State()
{
    State state;
    state.timeAtRead = timeAtRead;
    state.joulesAtRead = consumedEnergyDevice * 1e-3;
    return state;
}

NVMLPowerSensor* NVMLPowerSensor::create(
    const int device_number,
    const char *dumpFileName)
{
    return new NVMLPowerSensor_(device_number, dumpFileName);
}

NVMLPowerSensor_::NVMLPowerSensor_(
    const int device_number,
    const char *dumpFileName) :
    stop(false)
{

    char *cstr_nvml_id = getenv("NVML_ID");
    unsigned device_number_ = cstr_nvml_id ? atoi(cstr_nvml_id) : 0;

#if defined(HAVE_NVML)
    checkNVMLCall(nvmlInit());
    checkNVMLCall(nvmlDeviceGetHandleByIndex(device_number_, &device));
#endif

    previousState = read_nvml();
    previousState.consumedEnergyDevice = 0;

    if ((errno = pthread_create(&thread, 0, &NVMLPowerSensor_::IOthread, this)) != 0) {
        perror("pthread_create");
        exit(1);
    }
}


NVMLPowerSensor_::~NVMLPowerSensor_() {
    stop = true;
    if ((errno = pthread_join(thread, 0)) != 0) {
        perror("pthread_join");
    }

#if defined(HAVE_NVML)
    checkNVMLCall(nvmlShutdown());
#endif
}


void *NVMLPowerSensor_::IOthread(void *arg) {
    return static_cast<NVMLPowerSensor_ *>(arg)->IOthread();
}


NVMLPowerSensor_::NVMLState NVMLPowerSensor_::read_nvml() {
    NVMLState state;
    state.timeAtRead = omp_get_wtime();

#if defined(HAVE_NVML)
    unsigned int power;
    checkNVMLCall(nvmlDeviceGetPowerUsage(device, &power));

    state.instantaneousPower = power;
    state.consumedEnergyDevice = previousState.consumedEnergyDevice;
    float averagePower = (state.instantaneousPower + previousState.instantaneousPower) / 2;
    float timeElapsed = (state.timeAtRead - previousState.timeAtRead);
    state.consumedEnergyDevice += averagePower * timeElapsed;
#else
    state.consumedEnergyDevice = 0;
#endif

    return state;
}


void *NVMLPowerSensor_::IOthread() {
    NVMLState firstState = read_nvml(), currentState;

    while (!stop) {
        currentState = read_nvml();
        dump(firstState, previousState, currentState);
        previousState = currentState;
    }

    void *retval;
    pthread_exit(retval);
    return retval;
}

State NVMLPowerSensor_::read() {
    return read_nvml();
}

} // end namespace nvml
} // end namespace powersensor
