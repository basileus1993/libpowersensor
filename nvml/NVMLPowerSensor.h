#ifndef NVML_POWER_SENSOR_H_
#define NVML_POWER_SENSOR_H_

#include "PowerSensor.h"

namespace powersensor {
    namespace nvml {
        class NVMLPowerSensor : public PowerSensor {
            public:
                static NVMLPowerSensor* create(
                    const int device_number = 0,
                    const char *dumpFileName = NULL);
        };
    } // end namespace nvml
} // end namespace powersensor

#endif
