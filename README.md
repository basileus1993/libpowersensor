# PowerSensor
This is a collection of `PowerSensor` implementations, based on the original `PowerSensor` code, available at https://gitlab.com/astron-misc/PowerSensor

# Installation
```
mkdir build
cd build
cmake ..
make install
```

# Usage
Include the header file into your program, e.g.:
```
#include "powersensor.h"
```

All PowerSensor instances are contained in a namespace:
```
using namespace powersensor;
```

Depending on which PowerSensor implementations you have selected during the build, you can now initialize
any PowerSensor instance:
```
PowerSensor sensor = arduino::ArduinoPowerSensor::create("/dev/ttyACM0")
```

Next, you can start measuring power using the common api as specified in `PowerSensor.h`:
```
PowerSensor::State start = sensor.read();
...
PowerSensor::State stop = sensor.read();
std::cout << "The computation took " << PowerSensor::Joules(start, stop) << 'J' << std::endl;
```
