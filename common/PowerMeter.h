#include <iostream>
#include <thread>
#include <chrono>

#include "PowerSensor.h"

template<typename T>
void run()
{
    auto powersensor = T::create();
    auto start = powersensor->read();
    std::this_thread::sleep_for(std::chrono::seconds(5));
    auto end = powersensor->read();
    std::cout << "Runtime: " << powersensor::PowerSensor::seconds(start, end) << " s" << std::endl;
    std::cout << "Joules: " << powersensor::PowerSensor::Joules(start, end) << " Joules" << std::endl;
    std::cout << "Watt: " << powersensor::PowerSensor::Watt(start, end) << " Watt" << std::endl;
}