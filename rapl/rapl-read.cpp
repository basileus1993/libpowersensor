/*
    Read the RAPL values from the sysfs powercap interface

    Original code by Vince Weaver (vincent.weaver@maine.edu)
    http://web.eece.maine.edu/~vweaver/projects/rapl/rapl-read.c
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <inttypes.h>
#include <unistd.h>
#include <math.h>
#include <string.h>

#include "rapl-read.h"

/* Intel CPU models */
#define CPU_SANDYBRIDGE     42
#define CPU_SANDYBRIDGE_EP  45
#define CPU_IVYBRIDGE       58
#define CPU_IVYBRIDGE_EP    62
#define CPU_HASWELL         60  // 69,70 too?
#define CPU_HASWELL_EP      63
#define CPU_BROADWELL       61  // 71 too?
#define CPU_BROADWELL_EP    79
#define CPU_BROADWELL_DE    86
#define CPU_SKYLAKE         78  // 94 too?
#define CPU_CASCADELAKE     85
#define CPU_KNIGHTSLANDING  87


Rapl::Rapl()
{
    // Detect cpu and packages
    detect_cpu();
    detect_packages();

    // Check whether rapl works
    #if defined(DEBUG)
    measure();
    if (!rapl_supported) {
        fprintf(stderr, "RAPL not supported, power measurement will be unavailable.\n");
    }
    #endif

}


void Rapl::detect_cpu() {
    FILE *fff;
    int family,model=-1;
    char buffer[BUFSIZ],*result;
    char vendor[BUFSIZ];

    fff=fopen("/proc/cpuinfo","r");
    if (fff==NULL) {
        printf("Could not open /proc/cpuinfo\n");
        exit(EXIT_FAILURE);
    }

    while(1) {
        result=fgets(buffer,BUFSIZ,fff);
        if (result==NULL) break;

        if (!strncmp(result,"vendor_id",8)) {
            sscanf(result,"%*s%*s%s",vendor);

            if (strncmp(vendor,"GenuineIntel",12)) {
                printf("%s not an Intel chip\n",vendor);
                exit(EXIT_FAILURE);
            }
        }

        if (!strncmp(result,"cpu family",10)) {
            sscanf(result,"%*s%*s%*s%d",&family);
            if (family!=6) {
                printf("Wrong CPU family %d\n",family);
                exit(EXIT_FAILURE);
            }
        }

        if (!strncmp(result,"model",5)) {
            sscanf(result,"%*s%*s%d",&model);
        }
    }

    fclose(fff);

    #if defined(DEBUG)
    printf("CPU model: ");

    switch(model) {
        case CPU_SANDYBRIDGE:
            printf("Sandybridge");
            break;
        case CPU_SANDYBRIDGE_EP:
            printf("Sandybridge-EP");
            break;
        case CPU_IVYBRIDGE:
            printf("Ivybridge");
            break;
        case CPU_IVYBRIDGE_EP:
            printf("Ivybridge-EP");
            break;
        case CPU_HASWELL:
            printf("Haswell");
            break;
        case CPU_HASWELL_EP:
            printf("Haswell-EP");
            break;
        case CPU_BROADWELL:
            printf("Broadwell");
            break;
        case CPU_CASCADELAKE:
            printf("Cascade Lake");
            break;
        case CPU_KNIGHTSLANDING:
            printf("Knights Landing");
            break;
        default:
            printf("Unsupported model %d\n",model);
            model=-1;
            break;
    }

    printf("\n");
    #endif

    cpu_model = model;
} // end Rapl::detect_cpu


void Rapl::detect_packages() {
    char filename[BUFSIZ];
    FILE *fff;
    int package;
    int i;

    for(i=0;i<MAX_PACKAGES;i++) package_map[i]=-1;

    for(i=0;i<MAX_CPUS;i++) {
        sprintf(filename,"/sys/devices/system/cpu/cpu%d/topology/physical_package_id",i);
        fff=fopen(filename,"r");
        if (fff==NULL) break;
        fscanf(fff,"%d",&package);
        fclose(fff);

        if (package_map[package]==-1) {
            total_packages++;
            package_map[package]=i;
        }
    }

    total_cores=i;

    #if defined(DEBUG)
    printf("\tDetected %d cores in %d packages\n\n",
        total_cores,total_packages);
    #endif
} // end Rapl::detect_packages


double Rapl::measure() {
    char event_names[MAX_PACKAGES][NUM_RAPL_DOMAINS][256];
    char filenames[MAX_PACKAGES][NUM_RAPL_DOMAINS][256];
    char basename[MAX_PACKAGES][256];
    char tempfile[256];
    long long measurements[MAX_PACKAGES][NUM_RAPL_DOMAINS];
    int valid[MAX_PACKAGES][NUM_RAPL_DOMAINS];
    int i,j;
    FILE *fff;

    if (!rapl_supported) {
        return 0;
    }

    double consumed_energy_dram    = 0;
    double consumed_energy_package = 0;

    //total_packages = 2;
    /* /sys/class/powercap/intel-rapl/intel-rapl:0/ */
    /* name has name */
    /* energy_uj has energy */
    /* subdirectories intel-rapl:0:0 intel-rapl:0:1 intel-rapl:0:2 */
    for(j=0;j<total_packages;j++) {
        i=0;
        sprintf(basename[j],"/sys/class/powercap/intel-rapl/intel-rapl:%d",
            j);
        sprintf(tempfile,"%s/name",basename[j]);
        fff=fopen(tempfile,"r");
        if (fff==NULL) {
            rapl_supported = false;
            return 0;
        }
        fscanf(fff,"%s",event_names[j][i]);
        valid[j][i]=1;
        fclose(fff);
        sprintf(filenames[j][i],"%s/energy_uj",basename[j]);

        /* Handle subdomains */
        for(i=1;i<NUM_RAPL_DOMAINS;i++) {
            sprintf(tempfile,"%s/intel-rapl:%d:%d/name",
                basename[j],j,i-1);
            fff=fopen(tempfile,"r");
            if (fff==NULL) {
                valid[j][i]=0;
                continue;
            }
            valid[j][i]=1;
            fscanf(fff,"%s",event_names[j][i]);
            fclose(fff);
            sprintf(filenames[j][i],"%s/intel-rapl:%d:%d/energy_uj",
                basename[j],j,i-1);

        }
    }

    /* Gather measurements */
    for(j=0;j<total_packages;j++) {
        for(i=0;i<NUM_RAPL_DOMAINS;i++) {
            if (valid[j][i]) {
                fff=fopen(filenames[j][i],"r");
                if (fff==NULL) {
                    fprintf(stderr,"\tError opening %s!\n",filenames[j][i]);
                }
                else {
                    fscanf(fff,"%lld",&measurements[j][i]);
                    fclose(fff);
                }
            }
        }
    }

    // Compute package and dram power consumption from individual measurements
    for(j=0;j<total_packages;j++) {
        for(i=0;i<NUM_RAPL_DOMAINS;i++) {
            if (valid[j][i]) {
                char *event_name = event_names[j][i];
                long long value = measurements[j][i] * 1e-6;
                // RAPL_DOMAIN 0: package
                if (i == 0) {
                    consumed_energy_package += value;

                // RAPL_DOMAIN 1: dram
                } else if(i == 1) {
                    consumed_energy_dram += value;
                }
            }
        }
    }

    return consumed_energy_package + consumed_energy_dram;
} // end Rapl::measure
