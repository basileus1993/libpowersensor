#ifndef RAPL_POWER_SENSOR_H
#define RAPL_POWER_SENSOR_H

#include "PowerSensor.h"

namespace powersensor {
    namespace rapl {
        class RaplPowerSensor : public PowerSensor {
            public:
                static RaplPowerSensor* create(const char *dumpFileName = 0);
        };
    } // end namespace rapl
} // end namespace powersensor

#endif
