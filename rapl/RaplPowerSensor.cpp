#include <fstream>
#include <iostream>

#include <unistd.h>
#include <omp.h>

#include "RaplPowerSensor.h"
#include "rapl-read.h"

namespace powersensor {
namespace rapl {

class RaplPowerSensor_ : public RaplPowerSensor {
    public:
        RaplPowerSensor_(const char *dumpFileName);
        ~RaplPowerSensor_();

        State read();
        void mark(const State &, const char *name = 0, unsigned tag = 0);
        void mark(const State &start, const State &stop, const char *name = 0, unsigned tag = 0);

    private:
        Rapl rapl;
        pthread_t       thread;
        volatile bool   stop;
        static void     *IOthread(void *);
        void            *IOthread();
};

RaplPowerSensor* RaplPowerSensor::create(
    const char *dumpFileName)
{
    return new RaplPowerSensor_(dumpFileName);
}


RaplPowerSensor_::RaplPowerSensor_(const char *dumpFileName) :
    stop(false)
{
    State startState = read();

    if ((errno = pthread_create(&thread, 0, &RaplPowerSensor_::IOthread, this)) != 0) {
        perror("pthread_create");
        exit(1);
    }
}

RaplPowerSensor_::~RaplPowerSensor_() {
    stop = true;

    if ((errno = pthread_join(thread, 0)) != 0) {
        perror("pthread_join");
    }
}


void *RaplPowerSensor_::IOthread(void *arg) {
    return static_cast<RaplPowerSensor_ *>(arg)->IOthread();
}


void *RaplPowerSensor_::IOthread() {
    State firstState = read(), currentState = firstState, previousState;

    while (!stop) {
        usleep(100);
        previousState = currentState;
        currentState  = read();
        dump(firstState, previousState, currentState);
    }

    void *retval;
    pthread_exit(retval);
    return retval;
}


State RaplPowerSensor_::read() {
    State state;
    state.timeAtRead = omp_get_wtime();

    #pragma omp critical (power)
    {
        state.joulesAtRead = rapl.measure();
    }

    return state;
}

} // end namespace rapl
} // end namespace powersensor
