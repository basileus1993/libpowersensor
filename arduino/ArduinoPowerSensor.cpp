#include "ArduinoPowerSensor.h"

#include <omp.h>

#if defined(HAVE_POWERSENSOR)
/*
    Include original PowerSensor header file
    The _ prefixed aliases prevents class and
    namespace conflicts.
*/
#include POWERSENSOR_HEADER
using _PowerSensor = PowerSensor::PowerSensor;
using _State = PowerSensor::State;
double (&_seconds)(const _State&, const _State&) = PowerSensor::seconds;
double (&_Joules)(const _State&, const _State&, int) = PowerSensor::Joules;
#endif


namespace powersensor {
namespace arduino {

class ArduinoPowerSensor_ : public ArduinoPowerSensor {
    public:
#if defined(HAVE_POWERSENSOR)
        ArduinoPowerSensor_(const char *device, const char *dumpFileName);
        ~ArduinoPowerSensor_();
#endif

        State read();

#if defined(HAVE_POWERSENSOR)
    private:
        _PowerSensor* _powersensor;
        _State _firstState;
#endif
};

#if defined(HAVE_POWERSENSOR)
ArduinoPowerSensor* ArduinoPowerSensor::create(
    const char *device,
    const char *dumpFileName)
{
    return new ArduinoPowerSensor_(device, dumpFileName);
}

ArduinoPowerSensor_::ArduinoPowerSensor_(
    const char *device,
    const char *dumpFileName)
{
    _powersensor = new _PowerSensor(device);
    _powersensor->dump(dumpFileName);
    _firstState = _powersensor->read();
}

ArduinoPowerSensor_::~ArduinoPowerSensor_() {
    delete _powersensor;
}

powersensor::State ArduinoPowerSensor_::read() {
    _State _state = _powersensor->read();
    powersensor::State state;
    state.timeAtRead   = _seconds(_firstState, _state);
    state.joulesAtRead = _Joules(_firstState, _state, -1);
    return state;
}
#else
ArduinoPowerSensor* ArduinoPowerSensor::create(const char*, const char*)
{
    return new ArduinoPowerSensor_();
}

State ArduinoPowerSensor_::read() {
    State state;
    state.timeAtRead = omp_get_wtime();
    state.joulesAtRead = 0;
    return state;
}
#endif

} // end namespace arduino
} // end namespace powersensor
