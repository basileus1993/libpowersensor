#ifndef ARDUINO_POWER_SENSOR_H_
#define ARDUINO_POWER_SENSOR_H_

#include "PowerSensor.h"

namespace powersensor {
    namespace arduino {
        class ArduinoPowerSensor : public PowerSensor {
            public:
                static ArduinoPowerSensor* create(
                    const char *device,
                    const char *dumpFileName = NULL);
        };
    } // end namespace arduino
} // end namespace powersensor

#endif
